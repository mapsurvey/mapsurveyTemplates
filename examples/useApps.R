## use the cyclingSafety app
cyclingSafety()
cyclingSafety(city = "Park City, UT", center = c(-111.503, 40.652))

## use the perceptualRegions app
perceptualRegions()
perceptualRegions(region = "'the South'", region_of = "the United States")
